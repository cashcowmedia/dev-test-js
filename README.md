## NEW Setup
Just run **npm install** and then **npm start**

# Task
Create a simple one page view where you have sorted the json data (people.json) according to the instructions. Just use the technologies you feel comfortable so feel free to add packages or just simply go vanilla. Up to you. You can change everything except the data and how you order it. Mainly want to see how you would solve the problem in javascript so CSS doesn't really matter. I've just added bootstrap so you can use the base styles from there to display the data in a bit nicer format. Additional CSS really isn't necessary. 

## Arrange the data
There are 150 people in json data. Some are female and some are male. Separate the people based on their gender. You can for example create two separate tables, one for females and other for males. If you feel some other way to display the is better, then go for it. Example tables are hardcoded in html, but it is just an example how to display the data so remove it when you get the idea. Also: 

* Males and Females should be in alphabetical order based on their first name. 
* Friends should be in alphabetical order based on first name. 
* Not necessary but as a bonus you can create some sort of filtering option. 
