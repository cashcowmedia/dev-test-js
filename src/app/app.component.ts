import {Component, OnInit} from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { combineLatest, Observable } from 'rxjs';
import { debounceTime, map, startWith } from 'rxjs/operators';

import DATA from '../assets/people.json';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  people$: Observable<any[]>;
  filtersForm: FormGroup;
  searchCtrl: FormControl = new FormControl('');
  title = 'DEV Test';
  gender: string[] = ['Male', 'Female'];

  constructor(private formBuilder: FormBuilder) {
    this.createForms();
  }

  ngOnInit() {
    this.people$ = combineLatest(
      [DATA.people],
      this.filtersForm.valueChanges
        .pipe(startWith({
          gender: ''
        })),
      this.searchCtrl.valueChanges
        .pipe(startWith(''), debounceTime(200))
    )
      .pipe(map(([people, filterObj, searchTerm]) => {
        return people.filter(p => {
          const filter = {
            isGenderOk: filterObj.gender ? filterObj.gender === p.gender : true,
            isSearchOk: searchTerm ? p.first_name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1 : true
          };
          return Object.values(filter).every(item => item === true);
        });
      }));
  }

  createForms() {
    this.filtersForm = this.formBuilder.group({
      gender: new FormControl()
    });
  }
}
